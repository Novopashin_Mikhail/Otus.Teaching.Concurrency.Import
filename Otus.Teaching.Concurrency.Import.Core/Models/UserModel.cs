using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public static UserModel ConvertFromEntity(Customer entity)
        {
            return new UserModel
            {
                Email = entity.Email,
                Id = entity.Id,
                Phone = entity.Phone,
                FullName = entity.FullName
            };
        }
        
        public static Customer ConvertToEntity(UserModel model)
        {
            return new Customer
            {
                Id = model.Id,
                Email = model.Email,
                Phone = model.Phone,
                FullName = model.FullName
            };
        }

        public override bool Equals(object? obj)
        {
            if (!(obj is UserModel user2)) return false;
            if (this.Id != user2.Id) return false;
            if (this.Email != user2.Email) return false;
            if (this.Phone != user2.Phone) return false;
            return this.FullName == user2.FullName;
        }
    }
}