﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private string _path;
        private XmlSerializer _serializer;
        public XmlParser(string path)
        {
            _path = path;
            _serializer = new XmlSerializer(typeof(CustomersList));
        }
        public List<Customer> Parse()
        {
            CustomersList list;
            using (Stream reader = new FileStream(_path, FileMode.Open))
            {
                // Call the Deserialize method to restore the object's state.
                list = (CustomersList)_serializer.Deserialize(reader);          
            }
            
            //Parse data
            return list?.Customers ?? null;
        }
    }
}