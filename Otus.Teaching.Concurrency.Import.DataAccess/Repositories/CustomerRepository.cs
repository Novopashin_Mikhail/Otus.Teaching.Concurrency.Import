using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private DataContext _context;
        public CustomerRepository(DataContext context)
        {
            _context = context;
        }

        public Customer Get(int id)
        {
            return _context.Customers.FirstOrDefault(x => x.Id == id);
        }

        public async Task<Customer> GetAsync(int id)
        {
            return await _context.Customers.FirstOrDefaultAsync(x => x.Id == id);
        }
        
        public Customer AddCustomer(Customer customer)
        {
            return _context.Customers.Add(customer).Entity;
        }
        public async Task<Customer> AddCustomerAsync(Customer customer)
        {
            if (_context.Customers.Any(x => x.Id == customer.Id))
                return null;
            
            var customerAdded = await _context.Customers.AddAsync(customer);
            await this.SaveWithAttemptAsync();
            return customerAdded.Entity;
        }
        public void AddCustomers(List<Customer> customers)
        {
            _context.Customers.AddRange(customers);   
        }
        
        public async Task AddCustomersAsync(List<Customer> customers)
        {
            await _context.Customers.AddRangeAsync(customers);   
        }

        public void RemoveCustomer(Customer customer)
        {
            _context.Customers.Remove(customer);
            this.SaveWithAttempt();
        }

        public async Task<int> GetCustomersCountAsync()
        {
            return await _context.Customers.CountAsync();
        }

        public void RemoveSpecifyCustomers(IEnumerable<int> ids)
        {
            var customers = _context.Customers.Where(x => ids.Contains(x.Id));
            foreach (var customer in customers)
            {
                RemoveCustomer(customer);                
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
        
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}