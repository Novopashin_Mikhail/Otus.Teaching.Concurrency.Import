using System.IO;
using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class Configuration
    {
        public string PathProcessGenerator { get; set; }
        public string PathToGeneratedFileDirecotry { get; set; }
        public bool IsRunAsProcess { get; set; }
        public int CountThreads { get; set; }
        public int CountItemsGenerate { get; set; }
        
        public static Configuration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("settings.json");
            var config = builder.Build();
            return config.GetSection("configuration").Get<Configuration>();
        }
    }
}