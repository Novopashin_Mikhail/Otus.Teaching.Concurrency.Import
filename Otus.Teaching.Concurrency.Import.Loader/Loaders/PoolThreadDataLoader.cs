using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class PoolThreadDataLoader : IDataLoader
    {
        private List<Customer> _bigData;
        private int _countThreads;
        public PoolThreadDataLoader(int countThreads)
        {
            _countThreads = countThreads;
        }
        // 2:17
        public void LoadData(List<Customer> bigData)
        {
            _bigData = bigData;
            var countItemsOneThread = bigData.Count() / _countThreads;
            int start = 0;
            var list = new List<ThreadPoolSchedulerItem>();
            
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            while (start < _countThreads)
            {
                var threadPoolSchedulerItem =
                    new ThreadPoolSchedulerItem(start * countItemsOneThread, countItemsOneThread);
                list.Add(threadPoolSchedulerItem);
                ThreadPool.QueueUserWorkItem(HandleInThreadPool, threadPoolSchedulerItem);
                start++;
            }

            WaitHandle[] waitHandles = list.Select(x => x.WaitHandle).ToArray();
            WaitHandle.WaitAll(waitHandles);
            stopWatch.Stop();
            Console.WriteLine($"Handled queue in {stopWatch.Elapsed}...");
        }

        public Task LoadDataAsync(List<Customer> data)
        {
            throw new NotImplementedException();
        }

        private void HandleInThreadPool(object item)
        {
            var schedulerItem = item as ThreadPoolSchedulerItem;
            Load(schedulerItem);
            if (schedulerItem == null) return;
            var autoResetEvent = (AutoResetEvent) schedulerItem.WaitHandle;
            autoResetEvent.Set();
        }

        private void Load(ThreadPoolSchedulerItem data)
        {
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} START load Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
            var bigData = _bigData.Skip(data.Skip).Take(data.Take).ToList();
                
            var batchLimit = 25000;
            var countIteration = bigData.Count() / batchLimit;
            
            using var context = new DataContext();
            for (int x = 0; x < countIteration; x++)
            {
                SaveToDbRange(context, bigData.Skip(x * batchLimit).Take(batchLimit).ToList());
            }
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} END load Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
        }
        
        private void SaveToDbRange(DataContext context, List<Customer> customers)
        {
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} START SaveToDbRange Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
            var repository = new CustomerRepository(context);
            repository.AddCustomers(customers.ToList());
            repository.SaveWithAttempt();
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} END SaveToDbRange Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
        }
        
        class ThreadPoolSchedulerItem
        {
            public int Skip { get; private set; }
            public int Take { get; private set; }

            public WaitHandle WaitHandle { get; private set; }

            public ThreadPoolSchedulerItem(int skip, int take)
            {
                Skip = skip;
                Take = take;
                WaitHandle = new AutoResetEvent(false);
            }
        }
    }
}