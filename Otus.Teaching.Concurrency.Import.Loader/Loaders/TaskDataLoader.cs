using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class TaskDataLoader : IDataLoader
    {
        private int _countThreads;
        public TaskDataLoader(int countThreads)
        {
            _countThreads = countThreads;
        }

        public void LoadData(List<Customer> data)
        {
            throw new NotImplementedException();
        }

        public async Task LoadDataAsync(List<Customer> bigData)
        { 
            //время 2:16
            var data = bigData;
            var listTask = new List<Task>();
            for (var i = 0; i < _countThreads; i++)
            {
                var takeItems = data.Count / _countThreads; 
                var j = i;
                
                var taskFactory = new TaskFactory();
                var task = taskFactory.StartNew(() => LoadAsync(data.Skip(j * takeItems).Take(takeItems).ToList()));
                listTask.Add(task);
            }
            await Task.WhenAll(listTask);
        }

        private async Task LoadAsync(List<Customer> data)
        {
            Console.WriteLine($"Thread={Task.CurrentId} START load Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
            await using var context = new DataContext();
            var batchLimit = 25000;
            var countIteration = IDataLoader.GetCountIteration(data.Count(), batchLimit);
            for (int x = 0; x < countIteration; x++)
            {
                await SaveToDbRangeAsync(context, data.Skip(x * batchLimit).Take(batchLimit).ToList());
            }
            Console.WriteLine($"Thread={Task.CurrentId} END load Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
        }
        
        private async Task SaveToDbRangeAsync(DataContext context, List<Customer> customers)
        {
            Console.WriteLine($"Thread={Task.CurrentId} START SaveToDbRange Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
            var repository = new CustomerRepository(context);
            await repository.AddCustomersAsync(customers.ToList());
            await repository.SaveWithAttemptAsync();
            Console.WriteLine($"Thread={Task.CurrentId} END SaveToDbRange Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
        }
    }
}