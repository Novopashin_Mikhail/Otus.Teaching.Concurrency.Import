﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.XmlGenerator;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath;
        private static string _fileName = "Test";
        private static string _extension = ".csv";
        private static int _count = 1000000;
        private static IDataLoader _dataLoader;
        private static IDataParser<List<Customer>> _dataParser;
        private static Configuration _config;
        
        static async Task Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0]; 
            }
            else
            {
                _config = Configuration.GetConfiguration();
                _count = _config.CountItemsGenerate;
                if (_config.IsRunAsProcess)
                {
                    RunGenerateAsProcess();
                }
                else
                {
                    await RunGenerate();
                }
            }
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            var data = ParseData();
            await RunMigrate();
            Console.WriteLine($"Начало загрузки {DateTime.Now}");
            await LoadDataAsync(data);
        }

        private static async Task LoadDataAsync(List<Customer> data)
        {
            _dataLoader = new TaskDataLoader(_config.CountThreads);
            await _dataLoader.LoadDataAsync(data);
        }

        private static List<Customer> ParseData()
        {
            _dataParser = new CsvParser(_dataFilePath);
            var data = _dataParser.Parse();
            return data;
        }

        private static async Task RunGenerate()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"{_fileName}{_extension}");
            await GenerateCustomersDataFile(path);
            _dataFilePath = path;
        }

        private static void RunGenerateAsProcess()
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = {$"{_fileName}{_extension}", _count.ToString()},
                FileName = _config.PathProcessGenerator,
            };
            Process.Start(startInfo)?.WaitForExit();
            _dataFilePath = Path.Combine(_config.PathToGeneratedFileDirecotry, $"{_fileName}{_extension}");
        }

        private static async Task RunMigrate()
        {
            await using var context = new DataContext();
            await context.Database.MigrateAsync();
        }

        private static async Task GenerateCustomersDataFile(string path)
        {
            var generator = GeneratorFactory.GetCsvGenerator(path, _count);
            await generator.GenerateAsync();
        }
        
        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine($"Конец процесса {DateTime.Now}");
        }
    }
}