using System.Threading.Tasks;
using NUnit.Framework;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.Test
{
    public class GeneratorTests
    {
        [Test]
        public async Task GenerateCustomer()
        {
            var customers = await RandomCustomerGenerator.GenerateAsync(1);
            Assert.NotNull(customers, "Список не проинициализирован");
            Assert.IsNotEmpty(customers, "Список пуст");
            foreach (var customer in customers)
            {
                Assert.Greater(customer.Id, 0, "Id = 0");
                Assert.NotNull(customer.Email, "Email is null");
                Assert.AreNotEqual(customer.Email, string.Empty, "Email is empty");
                Assert.NotNull(customer.Phone, "Phone is null");
                Assert.AreNotEqual(customer.Phone, string.Empty, "Phone is empty");
                Assert.NotNull(customer.FullName, "FullName is null");
                Assert.AreNotEqual(customer.FullName, string.Empty, "FullName is empty");
            }
        }
    }
}