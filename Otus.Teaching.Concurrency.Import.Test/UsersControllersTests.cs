using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Moq;
using NUnit.Framework;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.Web.Api.Controllers;
using Otus.Teaching.Concurrency.Import.Web.Api.Service;

namespace Otus.Teaching.Concurrency.Import.Test
{
    public class UsersControllersTests
    {
        [Test]
        [TestCase(100,false, 404)]
        [TestCase(101,true, 200)]
        public async Task GetUserById(int userId, bool isCreateUser, int expectedCode)
        {
            var anyId = userId;
            UserModel user = null;
            if (isCreateUser) user = new UserModel();
            var mockUserService = new Mock<IUserService>();
            mockUserService
                .Setup(x => x.GetUserAsync(anyId))
                .ReturnsAsync(user);
            var userController = new UsersController(mockUserService.Object);
            var response = await userController.Get(anyId);
            var statusCodeResult = (IStatusCodeActionResult)response;
            Assert.AreEqual(expectedCode, statusCodeResult.StatusCode);
        }
        
        [Test]
        [TestCase(100,true, 409)]
        [TestCase(101,false, 200)]
        public async Task AddUser(int userId, bool isConflictAdded, int expectedCode)
        {
            UserModel addedUser = null;
            UserModel user = new UserModel
            {
                Id = userId
            };
            if (!isConflictAdded)
            {
                addedUser = user;
            };
            var mockUserService = new Mock<IUserService>();
            mockUserService
                .Setup(x => x.AddUserAsync(user))
                .ReturnsAsync(addedUser);
            var userController = new UsersController(mockUserService.Object);
            var response = await userController.Post(user);
            var statusCodeResult = (IStatusCodeActionResult)response;
            Assert.AreEqual(expectedCode, statusCodeResult.StatusCode);
        }
    }
}