﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.Web.Api.Service;

namespace Otus.Teaching.Concurrency.Import.Web.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("MyPolicy")]
    public class UsersController : ControllerBase
    {
        private IUserService _service;
        private Timer _dataValidationTimer;
        
        public UsersController(IUserService service)
        {
            _service = new UserServiceProxy(service);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var userModel = await _service.GetUserAsync(id);
                if (userModel == null)
                    return NotFound();
                return Ok(userModel);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserModel model)
        {
            try
            {
                var user = await _service.AddUserAsync(model);
                if (user == null)
                    return Conflict(); 
                return Ok(user);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}