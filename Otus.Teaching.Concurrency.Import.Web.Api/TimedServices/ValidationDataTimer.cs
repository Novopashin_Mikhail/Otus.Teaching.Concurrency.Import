using System;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Web.Api.Service;

namespace Otus.Teaching.Concurrency.Import.Web.Api.TimedServices
{
    public class ValidationDataTimer : TimedHostedService
    {
        private readonly ICustomerRepository _repository;
        
        public ValidationDataTimer(ICustomerRepository repository)
        {
            Timer = new Timer(DoWork, null, TimeSpan.Zero, 
                TimeSpan.FromSeconds(15)); // TODO 15 секунд для быстрой проверки
            _repository = repository;
        }
        protected override void DoWork(object state)
        {
            if(!Cache.UserModelDict.Any()) return;
            foreach(var currentUserModelKeyValue in Cache.UserModelDict)
            {
                var currentUserModelValue = currentUserModelKeyValue.Value;
                var currentUserModel = currentUserModelValue.UserModel;
                var customer = _repository.Get(currentUserModel.Id);
                var model = UserModel.ConvertFromEntity(customer);
                if (model.Email != currentUserModel.Email ||
                    model.Phone != currentUserModel.Phone ||
                    model.FullName != currentUserModel.FullName)
                {
                    currentUserModel = model;
                }
            }
        }
    }
}